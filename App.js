import React, { useState, useEffect } from "react";
import AppNavigator from "./src/navigation/AppNavigator";
import { AuthProvider } from "./src/provider/AuthProvider";
import { ThemeProvider } from "react-native-rapi-ui";
import { LogBox } from "react-native";
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import configureStore from "./src/store";

export default function App() {
  const [store, setStore] = useState("");

  const images = [
    require("./assets/icon.png"),
    require("./assets/splash.png"),
    require("./assets/login.png"),
    require("./assets/register.png"),
    require("./assets/forget.png"),
  ];

  useEffect(() => {
		// checkLogin();
    setStore(configureStore);
    LogBox.ignoreLogs([
      "AsyncStorage has been extracted from react-native core and will be removed in a future release. It can now be installed and imported from '@react-native-async-storage/async-storage' instead of 'react-native'. See https://github.com/react-native-async-storage/async-storage",
    ]);
	}, []);

  return (
    <ThemeProvider images={images}>
      <Provider store={store.store}>
        <PersistGate persistor={store.persistor}>
          <AuthProvider>
            <AppNavigator />
          </AuthProvider>
        </PersistGate>
      </Provider>
    </ThemeProvider>
  );
}
