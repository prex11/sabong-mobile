export function setBetting(betting: String) {
  return {
    type: 'SET_BETTING',
    betting,
  };
}