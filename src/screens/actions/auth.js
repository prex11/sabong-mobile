export function setUser(user: Object) {
  return {
    type: 'SET_USER',
    user,
  };
}

export function setAccessToken(access_token: String) {
  return {
    type: 'SET_ACCESS_TOKEN',
    access_token,
  };
}

export function setSessionExpired(session_expired: Boolean) {
  return {
    type: 'SET_SESSION_EXPIRED',
    session_expired,
  };
}

export function setGlobalHost(host: String) {
  return {
    type: 'SET_HOST',
    host,
  };
}
