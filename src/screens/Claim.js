import React, { useEffect, useState } from 'react';
import { View, 
	ScrollView,
	Alert,
  ActivityIndicator,
} from 'react-native';
import { 
	Layout, 
	Text,
	TopNav,
	useTheme,
  themeColor,
  TextInput,
  Button
} from 'react-native-rapi-ui';
import { connect } from 'react-redux';
import Jsona from 'jsona';
import axios from 'axios';
import { Ionicons } from "@expo/vector-icons";
import moment from 'moment';

import { setAccessToken } from './actions/auth.js';
import { setBetting } from './actions/betting.js';

const jsona = new Jsona();

const Claim =  ({ navigation, betting, user, setBetting }) => {
	const { isDarkmode } = useTheme();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => {
    })();
  }, []);


  buttonActivity = () => {
    if (loading) {
      return <View
        style={{
          flex: 1,
          flexDirection: "row"
        }}
      >
        <Text fontWeight="bold" style={{ color: themeColor.gray100, marginRight: 5 }}>{ "Processing.." }</Text>
        <ActivityIndicator size="small" color={ themeColor.gray100 } />
      </View>
      
      
    } else {
      return "Mark as Claimed"
    }
  }

  const claimBet = async () => {  

    try {
      setLoading(true);
      let data = {
        type: 'bettings',
        id: betting.id,
        claimed: 1
      }

      const payload = jsona.serialize({
        stuff: data,
        includeNames: [],
      });

      axios.patch('/bettings/' + betting.id + '?include=fight,fight.tournament,user', payload)
      .then(response => {
        setLoading(false);
        const data = jsona.deserialize(response.data)

        Alert.alert(
          "Success",
          "Successfully claimed payout."
        );
        navigation.goBack()

      })
      .catch(error => {
        setLoading(false);
      })

    } catch (error) {
      setLoading(false);
      Alert.alert(
        "Something went wrong.",
        "Review host or contact admin."
      );
    }

  }

	return (
		<Layout>
      <TopNav
        middleContent="Claim"
        leftContent={
          <Ionicons
            name="chevron-back"
            size={20}
            color={isDarkmode ? themeColor.white100 : themeColor.black}
          />
        }
        leftAction={() => navigation.goBack() }
      />
			<ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          backgroundColor: isDarkmode ? themeColor.dark : themeColor.white,
        }}
      >
				<View
          style={{
            flex: 3,
            paddingHorizontal: 20,
            paddingBottom: 20,
            paddingTop: 20,
          }}
        >
          <Text size="xl" style={{alignSelf: 'center'}} fontWeight='bold'>{ betting.fight.tournament.title } - Fight #{betting.fight.fight_number}</Text>
          
          <View style={{
            marginTop: 15,
            paddingTop: 8,
            paddingBottom: 8,
            alignItems: "center",
            justifyContent: "space-between",
            flexDirection: "row",
          }}>
            <Text size="md">Reference #:</Text>
            <Text fontWeight="bold">{betting.reference_number}</Text>
          </View>

          <View style={{
            paddingTop: 8,
            paddingBottom: 8,
            alignItems: "center",
            justifyContent: "space-between",
            flexDirection: "row",
            
            
          }}>
            <Text size="md" style={{ marginTop: 4 }}>Amount:</Text>
            <Text fontWeight="bold">{ (betting.fight.status === 'cancelled' || betting.fight.winner === 'draw') ? betting.payout : betting.amount}</Text>
          </View>

          <View style={{
            paddingTop: 8,
            paddingBottom: 8,
            alignItems: "center",
            justifyContent: "space-between",
            flexDirection: "row",
            
            
          }}>
            <Text size="md" style={{ marginTop: 4 }}>Station:</Text>
            <Text fontWeight="bold">{betting.user.name}</Text>
          </View>

          <View style={{
            paddingTop: 8,
            paddingBottom: 8,
            alignItems: "center",
            justifyContent: "space-between",
            flexDirection: "row",
            
            
          }}>
            <Text size="md" style={{ marginTop: 4 }}>Date:</Text>
            <Text fontWeight="bold">{ moment(betting.createdAt).format('MMM DD, H:mma') }</Text>
          </View>

          <View style={{
            paddingTop: 8,
            paddingBottom: 8,
            alignItems: "center",
            justifyContent: "space-between",
            flexDirection: "row",
            
            
          }}>
            <Text size="md" style={{ marginTop: 4 }}>Payout:</Text>
            <Text fontWeight="bold" style={{color: themeColor.success600}}>{betting.payout}</Text>
          </View>

          <View style={{
            paddingTop: 8,
            paddingBottom: 8,
            alignItems: "center",
            justifyContent: "space-between",
            flexDirection: "row",
            
            
          }}>
            <Text size="md" style={{ marginTop: 4 }}>Bet on:</Text>
            { (betting.ring_side === 'meron') ? <Text fontWeight="bold" style={{color: themeColor.primary500}}>Inilog</Text> : <Text fontWeight="bold" style={{color: themeColor.danger}}>Biya</Text> }
          </View>

          {
            (betting.fight.winner === 'draw' || betting.fight.status === 'cancelled') ?
              <View style={{
                paddingTop: 8,
                paddingBottom: 8,
                alignItems: "center",
                justifyContent: "space-between",
                flexDirection: "row",
                
                
              }}>
                <Text size="md" style={{ marginTop: 4 }}>Winner:</Text>
                <Text fontWeight="bold" style={{color: themeColor.gray}}> { betting.fight.winner === 'draw' ? 'Draw' : 'Cancelled' }</Text>
              </View>
              :
              <View style={{
                paddingTop: 8,
                paddingBottom: 8,
                alignItems: "center",
                justifyContent: "space-between",
                flexDirection: "row",
                
                
              }}>
                <Text size="md" style={{ marginTop: 4 }}>Winner:</Text>
                { (betting.fight.winner === 'meron') ? <Text fontWeight="bold" style={{color: themeColor.primary500}}>Inilog</Text> : <Text fontWeight="bold" style={{color: themeColor.danger}}>Biya</Text> }
              </View>
          }

          {
            ((betting.fight.winner === 'draw' || betting.fight.status === 'cancelled') || betting.ring_side === betting.fight.winner) ?
              <View style={{
                paddingTop: 8,
                paddingBottom: 8,
                alignItems: "center",
                justifyContent: "space-between",
                flexDirection: "row",
                
                
              }}>
                <Text size="md" style={{ marginTop: 4 }}>Status:</Text>
                {(betting.claimed) ? <Text fontWeight="bold" style={{color: themeColor.primary600}}>Claimed</Text> : <Text fontWeight="bold" style={{color: themeColor.warning500}}>Not Claimed</Text>}
              </View>
              :
              <View style={{
                paddingTop: 8,
                paddingBottom: 8,
                alignItems: "center",
                justifyContent: "space-between",
                flexDirection: "row",
                
                
              }}>
                <Text size="md" style={{ marginTop: 4 }}>Status: </Text>
                <Text fontWeight="bold" style={{color: themeColor.danger}}>Loss</Text>
              </View>
          }

          {
            (betting.user_id == user.id) ?

              (!betting.claimed && (betting.ring_side === betting.fight.winner || (betting.fight.winner === 'draw' || betting.fight.status === 'cancelled'))) ? <Button
                text={  buttonActivity() }
                onPress={() => {
                  claimBet()
                }}
                style={{
                  marginTop: 20,
                }}
                disabled={loading}
              /> : null

            :
            
              <View>
                <Text fontWeight="bold" style={{alignSelf: 'center', marginTop: 25, color: themeColor.danger}}>Betting was placed to another station. You cannot issue claim to this betting.</Text>
              </View>
          }
          
        </View>
			</ScrollView>
		</Layout>
	);
}


const mapStateToProps = (state) => ({
  user: state.authReducer.user,
  betting: state.bettingReducer.betting,
});

const mapDispatchToProps = (dispatch) => ({
  setToken: (data) => dispatch(setAccessToken(data)),
  setBetting: (data) => dispatch(setBetting(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Claim);
