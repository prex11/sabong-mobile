import React, { useEffect, useState } from 'react';
import { View, 
	ScrollView,
	Alert,
  ActivityIndicator,
	StyleSheet
} from 'react-native';
import { 
	Layout, 
	Text,
	themeColor, 
	TopNav,
	useTheme,
  TextInput,
  Button
} from 'react-native-rapi-ui';
import { useFocusEffect } from '@react-navigation/native';
import { Fab, Icon, NativeBaseProvider } from "native-base"
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { connect, useDispatch } from 'react-redux';
import Jsona from 'jsona';
import { BarCodeScanner } from 'expo-barcode-scanner';
import qs from "qs";
import axios from 'axios';
import { Audio } from 'expo-av';

import { setUser, setAccessToken } from './actions/auth.js';
import { setBetting } from './actions/betting.js';

const jsona = new Jsona();

const Home =  ({ navigation, access_token, setUser, setBetting, host }) => {
	const { isDarkmode } = useTheme();
  const [reference_number, setReferenceNumber] = useState("")
  const [loading, setLoading] = useState(false);
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [scan, setScan] = useState(false);
  const [sound, setSound] = React.useState();

  useEffect(() => {
    (async () => {
      // axios.defaults.baseURL = host + '/public/api/v1'
      axios.defaults.baseURL = 'http://api.sabongnisabas.com/api/v1'
      axios.defaults.headers.common['Authorization'] = "Bearer " + access_token

      
      setScan(false)
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();

    return sound
      ? () => {
          sound.unloadAsync(); }
      : undefined;
  }, [sound]);

  useFocusEffect(
    React.useCallback(() => {
      getProfile()
      setScan(false)
      setReferenceNumber(null)
    }, [])
  );



  const handleBarCodeScanned = async ({ type, data }) => {
    playSound()
    setScan(false);
    await setReferenceNumber(data)

    searchBet(data)
    
  };

  playSound = async () => {
    const { sound } = await Audio.Sound.createAsync(
      require('../../assets/music/barcode.mp3')
    );
    setSound(sound);
    
    sound.setVolumeAsync(0.3)
    await sound.playAsync();
  }

  buttonActivity = () => {
    if (loading) {
      return <View
        style={{
          flex: 1,
          flexDirection: "row"
        }}
      >
        <Text fontWeight="bold" style={{ color: themeColor.gray100, marginRight: 5 }}>{ "Searching Bet" }</Text>
        <ActivityIndicator size="small" color={ themeColor.gray100 } />
      </View>
      
      
    } else {
      return "Submit"
    }
  }

  const getProfile = async (ref_number) => {

    try {

      axios.get('/me?include=roles')
        .then(response => {
          let data = jsona.deserialize(response.data)
          setUser(data)
        })
        .catch(error => {
          setLoading(false);
        })
    
    } catch (error) {
      setLoading(false);
      Alert.alert(
        "Something went wrong.",
        "Review host or contact admin."
      );
    }

  }

  const searchBet = async (ref_number) => {

    let r_number = (ref_number) ? ref_number : reference_number

    if (!r_number) {
      Alert.alert(
        "Error",
        "Reference Number is required!"
      );
      return
    }
    

    try {

      setLoading(true);

      let params = {
        filter: {
          ...({ reference_number: r_number })
        }
      }

      const payload = {
        params: params,
        paramsSerializer: function (params) {
          return qs.stringify(params, { encode: false });
        },
      }

      axios.get('/bettings?include=fight,fight.tournament,user', payload)
        .then(response => {
          setLoading(false);
          const data = jsona.deserialize(response.data)
          
          if (!data.length) {
            Alert.alert(
              "Error",
              "Betting with reference " + r_number + " not found!"
            );

            return;
          }

          let betting = data[0]
          if (betting.fight.status !== 'finish' && betting.fight.status !== 'cancelled') {
            Alert.alert(
              "Error",
              "This fight is not yet finished!"
            );
          } else {
            setBetting(betting)
            navigation.navigate("Claim")
          }

        })
        .catch(error => {
          setLoading(false);
        })
    
    } catch (error) {
      setLoading(false);
      Alert.alert(
        "Something went wrong.",
        "Review host or contact admin."
      );
    }

  }

  renderScanner = () => {
    if (hasPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <BarCodeScanner
        onBarCodeScanned={handleBarCodeScanned}
        style={[StyleSheet.absoluteFillObject, styles.container]}
      >
        <View style={styles.layerTop} />
        <View style={styles.layerCenter}>
          <View style={styles.layerLeft} />
          <View style={styles.focused} />
          <View style={styles.layerRight} />
        </View>
        <View style={styles.layerBottom}>
          <Button
            text="Close"
            onPress={() => {
              setScan(false)
            }}
          />
        </View>

        
      </BarCodeScanner>
  
    );
  }

  if (scan) {
    return renderScanner()
  }

	return (
		<Layout>
      <TopNav
        middleContent="Cash Out"
      />
			<ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          backgroundColor: isDarkmode ? themeColor.dark : themeColor.white,
        }}
      >
				<View
          style={{
            flex: 3,
            paddingHorizontal: 20,
            paddingBottom: 20,
            paddingTop: 20,
          }}
        >
          <Text>Reference Number</Text>
          <TextInput
            containerStyle={{ marginTop: 15 }}
            placeholder="Enter Reference Number"
            value={reference_number}
            autoCapitalize="none"
            autoCompleteType="off"
            autoCorrect={false}
            onChangeText={(text) => setReferenceNumber(text)}
          />

          <Button
            text={  buttonActivity() }
            onPress={() => {
              searchBet()
            }}
            style={{
              marginTop: 20,
            }}
            disabled={loading}
          />
        </View>

        <NativeBaseProvider>
          <Fab
            placement="bottom-right"
            style={{ backgroundColor: themeColor.primary }}
            size="sm"
            icon={<Icon color="white" as={<MaterialCommunityIcons name="qrcode-scan" />} size="sm" />}
            onPress={() => {
              if (!loading) {
                setReferenceNumber(null)
                setScan(true)
              }
            }}
          />
        </NativeBaseProvider>
			</ScrollView>
		</Layout>
	);
}

const opacity = 'rgba(0, 0, 0, .6)';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  layerTop: {
    flex: 1,
    backgroundColor: opacity
  },
  layerCenter: {
    flex: 1,
    flexDirection: 'row'
  },
  layerLeft: {
    flex: 1,
    backgroundColor: opacity
  },
  focused: {
    flex: 10
  },
  layerRight: {
    flex: 1,
    backgroundColor: opacity
  },
  layerBottom: {
    flex: 1,
    backgroundColor: opacity,
    alignItems: "center",
    justifyContent: 'center'
  },
});

const mapStateToProps = (state) => ({
  access_token: state.authReducer.access_token,
  host: state.authReducer.host,
});

const mapDispatchToProps = (dispatch) => ({
  setUser: (data) => dispatch(setUser(data)),
  setToken: (data) => dispatch(setAccessToken(data)),
  setBetting: (data) => dispatch(setBetting(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
