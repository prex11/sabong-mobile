import React, { useEffect, useState } from 'react';
import { View, 
	ScrollView,
	Alert,
	Image } from 'react-native';
import { 
	Layout, 
	Text,
	themeColor, 
	TopNav,
	useTheme
} from 'react-native-rapi-ui';
import { MaterialIcons } from "@expo/vector-icons";
import { connect, useDispatch } from 'react-redux';
import Jsona from 'jsona';

import { setAccessToken } from './actions/auth.js';

const jsona = new Jsona();

const Profile =  ({ navigation, access_token, setToken }) => {
	const { isDarkmode } = useTheme();

	const dispatchAction = useDispatch()
	
	return (
		<Layout>
			<ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}
      >
				<TopNav
					middleContent="Profile"
					rightContent={
						<MaterialIcons
							name="logout"
							size={20}
							color={isDarkmode ? themeColor.white100 : themeColor.black}
						/>
					}
					rightAction={() => { 
						setToken(null)
						dispatchAction({type: 'RESET_AUTH'})
						dispatchAction({type: 'RESET'})
					} }
				/>
				<View
						style={{
							flex: 1,
							alignItems: 'center',
							justifyContent: 'center',
						}}
					>
					
					<Text>Coming Soon</Text>
				</View>
			</ScrollView>
		</Layout>
	);
}

const mapStateToProps = (state) => ({
  access_token: state.authReducer.access_token,
});

const mapDispatchToProps = (dispatch) => ({
  setToken: (data) => dispatch(setAccessToken(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
