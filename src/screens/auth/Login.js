import React, { useState, useEffect } from "react";
import {
  ScrollView,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
  Image,
  Alert
} from "react-native";
import axios from 'axios';
import { connect } from 'react-redux';

import { setAccessToken, setSessionExpired, setGlobalHost } from '../actions/auth.js';

import {
  Layout,
  Text,
  TextInput,
  Button,
  useTheme,
  themeColor,
} from "react-native-rapi-ui";


const Login = ({ navigation, access_token, setToken, session_expired, setSession, setGlobalHost }) => {
  const { isDarkmode, setTheme } = useTheme();
  const [host, setHost] = useState("http://192.168.1.10/backend/");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (session_expired) {
        setSession(false)
        Alert.alert(
          "Session Expired!",
          "Login again."
        );
      }
    });
	});

  async function login() {
    
    try {

      setLoading(true);
      let lhost = host.replace(/\/$/, ''); // will remove later
      axios.post('http://api.sabongnisabas.com/api/v1/login/mobile', {
          email: email,
          password: password,
      }).then(async (response) => {
        setLoading(false);
        const data = response.data
        
        // setGlobalHost(lhost)
        setToken(data.access_token)
      }).catch(error => {
        setLoading(false);
        const response = error.response
        if (response.status == 422) {
          Alert.alert(
            "Invalid input!",
            "Email and Password are required."
          );
        }

        if (response.status == 400) {
          let message = response.data.errors[0] ? response.data.errors[0].detail : null
          if (message == "User is not a station.") {
            Alert.alert(
              "Invalid login!",
              "You are not allowed."
            );
          } else {
            Alert.alert(
              "Invalid login!",
              "Email or Password is incorrect."
            );
          }
        }
      }) // end

    } catch (error) {
      setLoading(false);
      Alert.alert(
        "Something went wrong.",
        "Review host or contact admin."
      );
    }
  }

  return (
    <KeyboardAvoidingView behavior="height" enabled style={{ flex: 1 }}>
      <Layout>
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: isDarkmode ? "#17171E" : themeColor.white100,
            }}
          >
            <Image
              resizeMode="contain"
              style={{
                height: 220,
                width: 220,
              }}
              source={require("../../../assets/login.png")}
            />
          </View>
          <View
            style={{
              flex: 3,
              paddingHorizontal: 20,
              paddingBottom: 20,
              backgroundColor: isDarkmode ? themeColor.dark : themeColor.white,
            }}
          >
            <Text
              style={{
                alignSelf: "center",
                paddingTop: 30,
                color: "#00577a",
                paddingBottom: 30,
              }}
              size="h3"
            >
              Sabong ni <Text fontWeight="bold" size="h3" style={{ color: '#00577a' }}>Sabas</Text>
            </Text>
            {/* <Text>Host</Text>
            <TextInput
              containerStyle={{ marginTop: 15 }}
              placeholder="Enter host"
              value={host}
              autoCapitalize="none"
              autoCompleteType="off"
              autoCorrect={false}
              onChangeText={(text) => setHost(text)}
            /> */}

            <Text>Email</Text>
            <TextInput
              containerStyle={{ marginTop: 15 }}
              placeholder="Enter your email"
              value={email}
              autoCapitalize="none"
              autoCompleteType="off"
              autoCorrect={false}
              keyboardType="email-address"
              onChangeText={(text) => setEmail(text)}
            />

            <Text style={{ marginTop: 15 }}>Password</Text>
            <TextInput
              containerStyle={{ marginTop: 15 }}
              placeholder="Enter your password"
              value={password}
              autoCapitalize="none"
              autoCompleteType="off"
              autoCorrect={false}
              secureTextEntry={true}
              onChangeText={(text) => setPassword(text)}
            />
            <Button
              text={loading ? "Loading" : "Login"}
              onPress={() => {
                login();
              }}
              style={{
                marginTop: 20,
              }}
              disabled={loading}
            />
          </View>
        </ScrollView>
      </Layout>
    </KeyboardAvoidingView>
  );
}

const mapStateToProps = (state) => ({
  access_token: state.authReducer.access_token,
  session_expired: state.authReducer.session_expired,
});

const mapDispatchToProps = (dispatch) => ({
  setToken: (data) => dispatch(setAccessToken(data)),
  setSession: (data) => dispatch(setSessionExpired(data)),
  setGlobalHost: (data) => dispatch(setGlobalHost(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);