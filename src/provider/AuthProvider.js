import React, { createContext, useState, useEffect } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';

const AuthContext = createContext();
const AuthProvider = (props) => {
  // user null = loading
  const [user, setUser] = useState(null);

  useEffect(() => {
    checkLogin();
  }, []);

  async function checkLogin() {
    const value = await AsyncStorage.getItem('@access_token')
		if (value) {
			setUser(true);
			// getUserData();
		} else {
			setUser(false);
			// setUserData(null);
		}
  }

  return (
    <AuthContext.Provider
      value={{
        user,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export { AuthContext, AuthProvider };
