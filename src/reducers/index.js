import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import authReducer from "./Auth";
import bettingReducer from "./Betting";

export default combineReducers({
  form: formReducer,
  authReducer,
  bettingReducer
});
