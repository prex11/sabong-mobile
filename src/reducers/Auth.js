const initialState = {
  user: null,
  access_token: null,
  host: null,
  session_expired: false
};

export default function(state: any = initialState, action: Function) {
  switch (action.type) {
    case "SET_USER":
      return { ...state, user: action.user };
    case "SET_ACCESS_TOKEN":
      return { ...state, access_token: action.access_token };
    case "SET_HOST":
      return { ...state, host: action.host };
    case "SET_SESSION_EXPIRED":
      return { ...state, session_expired: action.session_expired };
    case "RESET_AUTH":
      return initialState;
    default:
      return state;
  }
}