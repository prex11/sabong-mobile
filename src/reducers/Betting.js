const initialState = {
  betting: null
};

export default function(state: any = initialState, action: Function) {
  switch (action.type) {
    case "SET_BETTING":
      return { ...state, betting: action.betting };
    default:
      return state;
  }
}