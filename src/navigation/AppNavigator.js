import React, { useContext } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useDispatch, useSelector } from 'react-redux'
import axios from "axios"

import { useTheme, themeColor } from "react-native-rapi-ui";
import TabBarIcon from "../components/utils/TabBarIcon";
import TabBarText from "../components/utils/TabBarText";
//Screens
import Home from "../screens/Home";
import Claim from "../screens/Claim";
import Profile from "../screens/Profile";
import Loading from "../screens/utils/Loading";
// Auth screens
import Login from "../screens/auth/Login";
import { AuthContext } from "../provider/AuthProvider";

const AuthStack = createNativeStackNavigator();
const Auth = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <AuthStack.Screen name="Login" component={Login} />
    </AuthStack.Navigator>
  );
};

const MainStack = createNativeStackNavigator();
const Main = () => {
  return (
    <MainStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <MainStack.Screen name="MainTabs" component={MainTabs} />
      <MainStack.Screen name="Claim" component={Claim} />
    </MainStack.Navigator>
  );
};

const Tabs = createBottomTabNavigator();
const MainTabs = () => {
  const { isDarkmode } = useTheme();
  return (
    <Tabs.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          borderTopColor: isDarkmode ? themeColor.dark100 : "#c0c0c0",
          backgroundColor: isDarkmode ? themeColor.dark200 : "#ffffff",
        },
      }}
    >
      {/* these icons using Ionicons */}
      <Tabs.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: ({ focused }) => (
            <TabBarText focused={focused} title="Cash Out" />
          ),
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} icon={"cash-outline"} />
          ),
        }}
      />
      <Tabs.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: ({ focused }) => (
            <TabBarText focused={focused} title="Profile" />
          ),
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} icon={"person"} />
          ),
        }}
      />
    </Tabs.Navigator>
  );
};

export default () => {
  const access_token = useSelector((state) => state.authReducer.access_token)
  const dispatch = useDispatch()


  axios.interceptors.request.use(function (config) {
    config.headers.common["Accept"] = "application/vnd.api+json";
    config.headers.common["Content-Type"] = "application/vnd.api+json";
    config.headers.common["content-type"] = "application/vnd.api+json";
    config.headers.post["content-type"] = "application/vnd.api+json";
    config.headers.patch["content-type"] = "application/vnd.api+json";
    config.headers.delete["content-type"] = "application/vnd.api+json";

  
    return config;
  });

  axios.interceptors.response.use(response => response, error => {
    const { status } = error.response

    if (status === 401) {
      dispatch({type: 'SET_ACCESS_TOKEN', access_token: null})
      dispatch({type: 'SET_SESSION_EXPIRED', session_expired: true})
    }

    return Promise.reject(error)
  })


  return (
    <NavigationContainer>
      { access_token ? <Main /> : <Auth />}
    </NavigationContainer>
  );
};
